import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Login from './pages/login/view.js'
import Register from './pages/register/view.js'
import Home from './pages/dashboard/home/view.js'
import Employee from './pages/dashboard/employee/view.js'
import EmployeeUpdate from './pages/dashboard/employeeUpdate/view.js'
import Notfound from './pages/notfound.js'

export default function RouteLink() {
  return (
    <Router>

      <Switch>

        <LoginRoute exact path="/">
          <Login />
        </LoginRoute>

        <Route path="/register">
          <Register />
        </Route>


        <PrivateRoute path="/dashboard">
          <Home />
        </PrivateRoute>
        <PrivateRoute path="/employee">
          <Employee />
        </PrivateRoute>
        <PrivateRoute path="/employee-update/:id">
          <EmployeeUpdate />
        </PrivateRoute>

        <Route path="*">
          <Notfound />
        </Route>
      </Switch>

    </Router>
  )
}

//lock route auth user logged in
function PrivateRoute({ children, ...rest }) {
  let isHasToken = localStorage.getItem('token')
  return (
    <Route {...rest} render={({ location }) => isHasToken ? (children)
      :
      (
        <Redirect to={{ pathname: "/", state: { from: location } }} />
      )
    } />
  );
}

function LoginRoute({ children, ...rest }) {
  let isHasToken = localStorage.getItem('token')
  return (
    <Route {...rest} render={({ location }) => isHasToken ?
      (<Redirect to={{ pathname: "/dashboard", state: { from: location } }} />)
      // (children)
      :
      (children)
    } />
  );
}