import React from 'react';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import PowerSettingsNewIcon from '@material-ui/icons/PowerSettingsNew';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import IconButton from '@material-ui/core/IconButton';
import useStyles from './style.js'
import { useHistory } from "react-router-dom";

export default function Sidebar(props) {
  const classes = useStyles();
  const history = useHistory();

  const handleDrawerClose = () => {
    props.dispatch({ type: 'ACTION_NAVBAR', payload: false })
  };

  const logout = () => {
    localStorage.removeItem('token')
    return history.push('/')
  }

  return (
    <Drawer variant="permanent" classes={{ paper: clsx(classes.drawerPaper, !props.state.open && classes.drawerPaperClose) }} open={props.state.open}>
      <div className={classes.toolbarIcon}>
        <IconButton onClick={() => handleDrawerClose()}>
          <ChevronLeftIcon />
        </IconButton>
      </div>

      <Divider />

      <List>
        <ListItem button onClick={() => window.location.href = '/dashboard'}>
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItem>
      </List>

      <Divider />

      <List>
        <ListItem button onClick={() => logout()}>
          <ListItemIcon>
            <PowerSettingsNewIcon />
          </ListItemIcon>
          <ListItemText primary="Keluar" />
        </ListItem>
      </List>

    </Drawer>
  )
}