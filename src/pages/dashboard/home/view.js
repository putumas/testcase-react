import React, { useEffect, useReducer, createRef } from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import useStyles from './style.js'
import Navbar from '../../../components/navbar/view.js'
import Sidebar from '../../../components/sidebar/view.js';
import StateReducer from '../../../states/initial.js'
import StateAction from '../../../states/action.js'
import MaterialTable from 'material-table'
import env from '../../../config.js'
import moment from 'moment'

export default function Dashboard() {
  //--------declare variables---------//
  const classes = useStyles();
  const [state, dispatch] = useReducer(StateAction, StateReducer)
  const url = env().api_url

  useEffect(() => {
    var user = localStorage.getItem('user')
    dispatch({ type: 'TITLE_NAVBAR', payload: JSON.parse(user).name })
  }, [])

  //--------function action---------//
  const btnAction = (data) => {
    window.location.href = "/employee-update/" + data.id
  }

  const refreshToken = () => {
    fetch(url + 'token-refresh', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        refresh_token: localStorage.getItem('refresh_token')
      }),
    }).then(response => response.json())
      .then(result => {
        if (result.status == 200) {
          localStorage.setItem('token', result.data.token.access_token)
          localStorage.setItem('refresh_token', result.data.token.refresh_token)
        }
      })
  }

  const btnDelete = (data) => {
    fetch(url + 'demo/employee/' + data.id, {
      method: 'delete',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      },
    }).then(response => response.json())
      .then(result => {
        console.log(result)
        tableRef.current && tableRef.current.onQueryChange()
        if (result.status == 200) {
          alert("sukses menghapus data")
        } else {
          alert('gagal')
        }
      })
  }


  //----------table props declaration----------//

  const tableRef = createRef()

  const searchData = (query, resolve) => {
    fetch(url + 'demo/employee/page-search', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      },
      body: JSON.stringify({
        query: {
          value: query.search
        },
        start_birth_date: "1982-10-10 00_00_00",
        end_birth_date: "1982-10-10 00_00_00",
        pagination: {
          page: query.page + 1,
          perpage: query.pageSize
        },
        sort: {
          sort: "ASC",
          field: 'id'
        }
      }),
    }).then(response => response.json())
      .then(result => {
        if (result.hasOwnProperty("error")) {
          refreshToken()
        } else {
          resolve({
            data: result.rows,
            page: result.meta.page - 1,
            totalCount: result.meta.total,
          })
        }
      })
  }

  const getData = (query, resolve) => {
    if (query.search) {
      console.log(query.search)
      searchData(query, resolve)
    } else {
      fetch(url + 'demo/employee/page-search', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
          query: {
            value: ""
          },
          start_birth_date: "1982-10-10 00_00_00",
          end_birth_date: "1982-10-10 00_00_00",
          pagination: {
            page: query.page + 1,
            perpage: query.pageSize
          },
          sort: {
            sort: "ASC",
            field: 'id'
          }
        }),
      }).then(response => response.json())
        .then(result => {
          if (result.hasOwnProperty("error")) {
            refreshToken()
          } else {
            resolve({
              data: result.rows,
              page: result.meta.page - 1,
              totalCount: result.meta.total,
            })
          }
        })
    }
  }

  return (
    <div className={classes.root}>
      <Navbar state={state} dispatch={dispatch} />
      <Sidebar state={state} dispatch={dispatch} />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>

          <Grid container spacing={3}>
            <Grid item xs={12}>

              <MaterialTable
                tableRef={tableRef}
                title="Tabel Employee"
                columns={[
                  { title: 'NIP', field: 'nip' },
                  { title: 'Full Name', field: 'full_name' },
                  { title: 'Address', field: 'address' },
                  { title: 'Email', field: 'email' },
                  { title: 'Birth Date', field: 'birth_date', render: rowData => (<p>{moment(new Date(rowData.birth_date)).format("DD/MM/YYYY")}</p>) },
                ]}
                data={query => new Promise((resolve) => { getData(query, resolve) })}
                actions={[
                  {
                    icon: 'refresh',
                    tooltip: 'Refresh Data',
                    isFreeAction: true,
                    onClick: () => tableRef.current && tableRef.current.onQueryChange(),
                  },
                  {
                    icon: 'add',
                    tooltip: 'Tambah Data',
                    isFreeAction: true,
                    onClick: () => window.location.href = "/employee",
                  },
                  {
                    icon: 'edit',
                    tooltip: 'Edit User',
                    onClick: (event, rowData) => btnAction(rowData)
                  },
                  {
                    icon: 'delete',
                    tooltip: 'Delete User',
                    onClick: (event, rowData) => btnDelete(rowData)
                  }
                ]}
                options={{
                  actionsColumnIndex: -1,
                  pageSizeOptions: [10, 25, 50],
                  pageSize: 10,
                  showFirstLastPageButtons: false,
                  sorting: false,
                }}
              />

            </Grid>
          </Grid>

        </Container>
      </main>
    </div>
  );
}