import React, { useEffect, useReducer, useState } from 'react'
import Navbar from '../../../components/navbar/view.js'
import Sidebar from '../../../components/sidebar/view.js'
import StateReducer from '../../../states/initial.js'
import StateAction from '../../../states/action.js'
import useStyles from './style.js'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputIcon from '@material-ui/icons/Input';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardTimePicker, KeyboardDatePicker, } from '@material-ui/pickers';
import env from '../../../config.js'
import moment from 'moment'
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';

export default function Employee() {
  //--------declare variables---------//
  const [state, dispatch] = useReducer(StateAction, StateReducer)
  const classes = useStyles()
  const url = env().api_url
  const [isLoading, setIsLoading] = useState(false)

  const [nip, setnip] = useState('')
  const [fullName, setfullName] = useState('')
  const [nickName, setnickName] = useState('')
  const [birthDate, setbirthDate] = useState('')
  const [address, setaddress] = useState('')
  const [phone, setphone] = useState('')
  const [mobile, setmobile] = useState('')
  const [email, setemail] = useState('')

  useEffect(() => {
    var user = localStorage.getItem('user')
    dispatch({ type: 'TITLE_NAVBAR', payload: JSON.parse(user).name })
  }, [])

  //-------function action------//

  const refreshToken = () => {
    fetch(url + 'token-refresh', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        refresh_token: localStorage.getItem('refresh_token')
      }),
    }).then(response => response.json())
      .then(result => {
        if (result.status == 200) {
          localStorage.setItem('token', result.data.token.access_token)
          localStorage.setItem('refresh_token', result.data.token.refresh_token)
        }
      })
  }

  const doSave = () => {
    if (birthDate && nip && fullName && nickName && birthDate && address && phone && mobile && email) {
      var birth = moment(new Date(birthDate)).format("YYYY/MM/DD")
      setIsLoading(true)
      fetch(url + 'demo/employee', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        body: JSON.stringify({
          nip: nip,
          full_name: fullName,
          nick_name: nickName,
          birth_date: birth,
          address: address,
          phone: phone,
          mobile: mobile,
          email: email
        }),
      }).then(response => response.json())
        .then(result => {
          setIsLoading(false)
          if (result.hasOwnProperty("error")) {
            refreshToken()
          } else {
            if (result.status == 200) {
              alert("Sukses Menambahkan Data")
              window.location.href = "/dashboard"
            } else if (result.status == 400) {
              alert("mohon periksa kembali data Anda!")
            } else {
              alert("terjadi kesalahan!")
            }
          }
        })
    } else {
      alert("lengkapi data terlebih dahulu!")
    }
  }

  return (
    <div className={classes.root}>
      <Navbar state={state} dispatch={dispatch} />
      <Sidebar state={state} dispatch={dispatch} />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Backdrop className={classes.backdrop} open={isLoading}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                <React.Fragment>
                  <Typography component="h2" variant="h6" color="primary" gutterBottom>Tambah Data</Typography> <br />
                  <Grid item xs={12} md={6} lg={6}>
                    <TextField value={nip} onChange={(e) => setnip(e.target.value)} variant="outlined" margin="normal" fullWidth label="NIP" />
                    <TextField value={fullName} onChange={(e) => setfullName(e.target.value)} variant="outlined" margin="normal" fullWidth label="Full Name" />
                    <TextField value={nickName} onChange={(e) => setnickName(e.target.value)} variant="outlined" margin="normal" fullWidth label="Nick Name" />
                    <TextField value={email} onChange={(e) => setemail(e.target.value)} variant="outlined" margin="normal" fullWidth label="Email" />
                    <TextField value={phone} onChange={(e) => setphone(e.target.value)} variant="outlined" margin="normal" fullWidth label="Phone Number" type="number" />
                    <TextField value={mobile} onChange={(e) => setmobile(e.target.value)} variant="outlined" margin="normal" fullWidth label="Mobile Number" type="number" />
                    <TextField value={address} onChange={(e) => setaddress(e.target.value)} variant="outlined" margin="normal" fullWidth label="Address" multiline />
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        margin="normal"
                        label="Tanggal Lahir"
                        format="dd/MM/yyyy"
                        value={birthDate == '' ? new Date('2017-08-18') : birthDate}
                        onChange={(date) => setbirthDate(date)}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                    <br /><br />
                    <Button startIcon={<InputIcon />} onClick={() => doSave()} type="button" fullWidth variant="contained" color="primary" className={classes.submit}>Submit</Button>
                  </Grid>
                </React.Fragment>

              </Paper>
            </Grid>
          </Grid>

        </Container>
      </main>
    </div>
  )
}