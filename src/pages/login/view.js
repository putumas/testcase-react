import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import InputIcon from '@material-ui/icons/Input';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import style from './style.js'
import env from '../../config.js'
// import Link from '@material-ui/core/Link';
import { Link, useHistory } from "react-router-dom";

export default function View() {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setIsLoading] = useState(false);
  const url = env().api_url
  const classes = style();
  const history = useHistory();

  const doLogin = () => {
    if (email == '' || password == '') {
      alert('lengkapi data!')
    } else {
      setIsLoading(true)
      fetch(url + 'login', {
        method: 'post',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          identity: email,
          password: password
        }),
      }).then((response) => response.json())
        .then((responseJson) => {
          // console.log(responseJson)
          setIsLoading(false)
          if (responseJson.status == 200) {
            localStorage.setItem('token', responseJson.data.token.access_token)
            localStorage.setItem('refresh_token', responseJson.data.token.refresh_token)
            localStorage.setItem('user', JSON.stringify(responseJson.data.user.roles[0]))
            history.push("/dashboard");
            // window.location.href = "/dashboard"
          } else if (responseJson.status == 400) {
            alert(responseJson.message)
          } else {
            alert("terjadi kesalahan!")
          }
        })
        .catch((error) => {
          console.log(error)
        })
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <Backdrop className={classes.backdrop} open={isLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>

      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">Login Disini</Typography>
        <div className={classes.form}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Email/Username"
            autoComplete="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            onKeyDown={(e) => e.key === 'Enter' ? doLogin() : ''}
            autoFocus
          />

          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Password"
            type="password"
            autoComplete="current-password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            onKeyDown={(e) => e.key === 'Enter' ? doLogin() : ''}
          />

          <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Remember me" />

          <Button startIcon={<InputIcon />} onClick={() => doLogin()} type="button" fullWidth variant="contained" color="primary" className={classes.submit}>Masuk</Button>

          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">Forgot password?</Link>
            </Grid>
            <Grid item>
              <Link to="/register">{"Don't have an account? Sign Up"}</Link>
              {/* <Link href="/register" variant="body2">{"Don't have an account? Sign Up"}</Link> */}
            </Grid>
          </Grid>
        </div>
      </div>
      <Box mt={8}>
        <Typography variant="body2" color="textSecondary" align="center">
          {'Copyright © '}<Link color="inherit" href="https://material-ui.com/">Your Website</Link>{' '}{new Date().getFullYear()}{'.'}
        </Typography>
      </Box>
    </Container>
  );
}