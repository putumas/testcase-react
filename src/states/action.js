export default function apiReducer(state, action) {
  switch (action.type) {
    case "TITLE_NAVBAR":
      return { ...state, title: action.payload };
    case "ACTION_NAVBAR":
      return { ...state, open: action.payload };
    default:
      return state;
  }
}