export default function environment() {

  const env = {
    api_url: 'https://127.0.0.2:443/api/',
  };

  return env
}